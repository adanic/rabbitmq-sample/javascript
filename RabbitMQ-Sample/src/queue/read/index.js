const rabbitmq = require('../../connection')
const fs = require('fs')

module.exports.consume = async (queueName) => {
  try {
    const channel = rabbitmq.getChannel()
    if (channel) {
      channel.prefetch(1);
      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queueName);
      channel.consume(queueName, function (msg) {
        let secs = msg.content.toString().split('.').length - 1;

        let date = new Date();
        const fileName = `${queueName}-${date.getTime()}.txt`
        console.log(" [x] Received Message is:   %s", msg.content.toString());
        fs.writeFile(`msg-files/${fileName}`, msg.content.toString(), function (err) {
          if (err) throw err
          console.log(`File ${fileName} is created successfully.`);
        })
        setTimeout(function () {
          channel.ack(msg);
        }, secs * 1000);
      }, {
        noAck: false
      });
    } else {
      console.log('### rabbitmq => channel is not valid')
    }
  } catch (err) {
    console.log('### error in receive message from queue... ', err)
  }
}
