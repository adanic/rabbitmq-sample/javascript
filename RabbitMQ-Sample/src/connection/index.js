const amqp = require('amqplib')

let channel

var connectToRabbitMq = async (url, username, password) => {
  try {
    const opt = {credentials: amqp.credentials.plain(username, password)}
    let open = amqp.connect(url, opt)
    channel = await open.then(function (conn) {
      return conn.createChannel()
    })
    return channel
  } catch (err) {
    console.log('### error in create connection to rabbitmq...', err)
    return 'error'
  }
}

process.on('exit', (code) => {
  if (channel) { channel.close() }
  console.log('Closing rabbitmq channel')
})

module.exports = {
  connect: connectToRabbitMq,
  getChannel: () => {
    return channel
  }
}
